
require s7plc
require essioc
require rflps_sim, develop
#require rflps_sim

############################################################################
# ESS IOC configuration
############################################################################
epicsEnvSet(IOCNAME, "SPK-080RFC:Ctrl-IOC-203")
epicsEnvSet(IOCDIR, "SPK-080RFC_Ctrl-IOC-203")
epicsEnvSet(LOG_SERVER_NAME, "172.16.107.59")
iocshLoad("$(essioc_DIR)/common_config.iocsh")

############################################################################
# RFLPS SIM Configuration
############################################################################
epicsEnvSet("PREFIX", "$(RFLPS_PREFIX=SPK-080:)")

epicsEnvSet("LOCATION", "SPK-080")
epicsEnvSet("ENGINEER", "Rafael Montano <rafael.montano@ess.eu>"

#var s7plcDebug 5

## Datablocks
epicsEnvSet("TCPPORTCPU", "3000")
epicsEnvSet("PLCPORTCPU", "PLCCPU")
epicsEnvSet("INSIZECPU", "8")
epicsEnvSet("OUTSIZECPU", "4")

epicsEnvSet("TCPPORTAF", "3001")
epicsEnvSet("PLCPORTAF", "PLCAF")
epicsEnvSet("INSIZEAF", "352")
epicsEnvSet("OUTSIZEAF", "208")

epicsEnvSet("TCPPORTDIO", "3002")
epicsEnvSet("PLCPORTDIO", "PLCDIO")
epicsEnvSet("INSIZEDIO", "192")
epicsEnvSet("OUTSIZEDIO", "32")

epicsEnvSet("TCPPORTPSU", "3003")
epicsEnvSet("PLCPORTPSU", "PLCPSU")
epicsEnvSet("INSIZEPSU", "390")
epicsEnvSet("OUTSIZEPSU", "222")

epicsEnvSet("PLCIP", "172.30.6.241")

s7plcConfigure("$(PLCPORTCPU)","$(PLCIP)",$(TCPPORTCPU),$(INSIZECPU),$(OUTSIZECPU),1,2500,500)
s7plcConfigure("$(PLCPORTAF)","$(PLCIP)",$(TCPPORTAF),$(INSIZEAF),$(OUTSIZEAF),1,2500,500)
s7plcConfigure("$(PLCPORTDIO)","$(PLCIP)",$(TCPPORTDIO),$(INSIZEDIO),$(OUTSIZEDIO),1,2500,500)
s7plcConfigure("$(PLCPORTPSU)","$(PLCIP)",$(TCPPORTPSU),$(INSIZEPSU),$(OUTSIZEPSU),1,2500,500)

dbLoadTemplate("$(E3_CMD_TOP)/subs/rflpsCPU.substitutions", "PREFIX=$(PREFIX), KLY=2")
dbLoadTemplate("$(E3_CMD_TOP)/subs/rflpsAF.substitutions", "PREFIX=$(PREFIX), KLY=2")
dbLoadTemplate("$(E3_CMD_TOP)/subs/rflpsDIO.substitutions", "PREFIX=$(PREFIX), KLY=2")
dbLoadTemplate("$(E3_CMD_TOP)/subs/rflpsPSU.substitutions", "PREFIX=$(PREFIX), KLY=2")


iocInit()


